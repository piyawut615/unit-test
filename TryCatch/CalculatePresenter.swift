import Foundation

enum CalculateError: Error{
    case stringEmpty
    case invalidNumber
}

class CalculatePresenter {
    
    func plusFunction(firstInput: String , secondInput: String) throws -> Int {
        if firstInput == "" || secondInput == "" {
            throw CalculateError.stringEmpty
        }
        let numberFirstInput: Int? = Int(firstInput) ?? nil
        let numberSecondInput: Int? = Int(secondInput) ?? nil

        if numberFirstInput == nil || numberSecondInput == nil {
            throw CalculateError.invalidNumber
        }
        return numberFirstInput! + numberSecondInput!
    }
}
