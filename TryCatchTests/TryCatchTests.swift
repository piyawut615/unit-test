
import XCTest
@testable import TryCatch

class TryCatchTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPlus_25_and_30_expect55(){
        let presenter = CalculatePresenter()
        do {
            let result = try presenter.plusFunction(firstInput: "25", secondInput: "30");
            XCTAssertEqual(result, 55)
        }
        catch CalculateError.stringEmpty {
            XCTFail()
        }
        catch CalculateError.invalidNumber {
            XCTFail()
        }
        catch {
            XCTFail()
        }
    }
    
    func testPlus_whenInputIncorrectFormat_expectThrowInvalidNumber(){
        let presenter = CalculatePresenter()
        do {
            let _ = try presenter.plusFunction(firstInput: "swift", secondInput: "30");
            XCTFail()
        }
        catch CalculateError.stringEmpty {
            XCTFail()
        }
        catch CalculateError.invalidNumber {
            XCTAssert(true)
        }
        catch {
            XCTFail()
        }
    }
    
    func testPlus_whenInoutIsEmpty_expectThrowStringEmpty(){
        let presenter = CalculatePresenter()
        do {
            let _ = try presenter.plusFunction(firstInput: "30", secondInput: "");
            XCTFail()
        }
        catch CalculateError.stringEmpty {
            XCTAssert(true)
        }
        catch CalculateError.invalidNumber {
            XCTFail()
        }
        catch {
            XCTFail()
        }
    }
    
}
